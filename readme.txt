=== squat-radar ===
Tags: calendar,events
Requires at least: 4.0
Tested up to: 5.1
Requires PHP: 5.4.0
Stable tag: 2.0.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Provides integration with https://radar.squat.net/ to display events on your Wordpress website.

== Installation ==
Enable the plugin as normal for your system.

Once activated:
1. Visit the Widgets page. Here you will now see \'Squat Radar Events\' widget, and the \'Squat Radar Shortcode\' sidebar.
2. Add the widget to either the sidebar you want, or if you want to use the shortcode in content, the \'Squat Radar Shortcode\' sidebar.
3. To configure the sidebar go to https://radar.squat.net/events and filter for the events you want to show. Maybe your city and group, or a category etc.
4. Once you have the events filter you want copy the address from your address bar into the widget.
5. Select which fields you would like to show.

If you put the widget in a displayed sidebar that\'s it. The filtered list of upcoming events will now show up.
If you used the Shortcode sidebar, add [squat_radar_sidebar] to the content where you want the events to display.
